﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace CycleAssignment
{
    public partial class PieChart : Form
    {

        String type, Fname = "";
        int hr1, hr2, hr3, hr4, hr5, hr6, min, max, maxhr;
        int z1, z2, z3, z4, z5, z6 = 0;
        Form1 data;
        public PieChart(string name, int xmin, int xmax, int maxheart, string state)
        {
            InitializeComponent();
            Fname = name;
            min = xmin;
            max = xmax;
            maxhr = maxheart;
            type = state;

            if (state == "heart")
            {
                PieSlices();
            }
            else if (state == "power")
            {
                PowerSlices();
            }
        }

        private void PieChart_Load(object sender, EventArgs e)
        {

        }

        private void LabelsPower()
        {
            lblzone1.Text = "Active Recovery <56%"; label10.Text = "'Easy spinning' or 'light pedal pressure', i.e., very low level exercise, too low in and of itself to induce significant physiological adaptations. ";
            lblzone2.Text = "Endurance 56-75%"; lblzone2text.Text = "'All day' pace, or classic long slow distance (LSD) training. Sensation of leg effort/fatigue generally low, but may rise periodically to higher levels (e.g., when climbing). ";
            lblzone3.Text = "Tempo 76-90%"; lblzone3text.Text = "Typical intensity of fartlek workout, 'spirited' group ride, or briskly moving paceline. More frequent/greater sensation of leg effort/fatigue than at level 2. ";
            lblzone4.Text = "Lactate Threshold 91-105%"; lblzone4text.Text = "Just below to just above TT effort, taking into account duration, current fitness, environmental conditions, etc. Essentially continuous sensation of moderate or even greater leg effort/fatigue.";
            lblzone5.Text = "VO2 Max 106-120%"; lblzone5text.Text = "Typical intensity of longer (3-8 min) intervals intended to increase VO2max. Strong to severe sensations of leg effort/fatigue, such that completion of more than 30-40 min total training time is difficult at best. ";
            lblZone6.Text = "Anaerobic Capacity >121%"; lblzone6text.Text = "Short (30 s to 3 min), high intensity intervals designed to increase anaerobic capacity. Heart rate generally not useful as guide to intensity due to non-steady-state nature of effort.";

            label10.Font = new Font("Microsoft Sans Serif", 8);
            lblzone2text.Font = new Font("Microsoft Sans Serif", 8);
            lblzone3text.Font = new Font("Microsoft Sans Serif", 8);
            lblzone4text.Font = new Font("Microsoft Sans Serif", 8);
            lblzone5text.Font = new Font("Microsoft Sans Serif", 8);
            lblzone6text.Font = new Font("Microsoft Sans Serif", 8);

        }
        private void PowerSlices()
        {
            LabelsPower();
            int rowcount, heartrate = 0;
            double hr, maxh;
            data = new Form1(Fname);
            maxh = Convert.ToDouble(maxhr);

            //http://www.bikeradar.com/gear/article/heart-rate-monitor-training-for-cyclists-28838/
            //z1 60-65%
            hr = maxh / 100;
            hr = hr * 0;
            hr = Math.Round(hr, 0);
            hr1 = Convert.ToInt32(hr);

            //z2 65-75
            hr = maxh / 100;
            hr = hr * 56;
            hr = Math.Round(hr, 0);
            hr2 = Convert.ToInt32(hr);

            //z3 75, 82
            hr = maxh / 100;
            hr = hr * 76;
            hr = Math.Round(hr, 0);
            hr3 = Convert.ToInt32(hr);

            //z4 82, 89
            hr = maxh / 100;
            hr = hr * 91;
            hr = Math.Round(hr, 0);
            hr4 = Convert.ToInt32(hr);

            //z5 89 - 94
            hr = maxh / 100;
            hr = hr * 106;
            hr = Math.Round(hr, 0);
            hr5 = Convert.ToInt32(hr);

            //z6 94-100
            hr = maxh / 100;
            hr = hr * 121;
            hr = Math.Round(hr, 0);
            hr6 = Convert.ToInt32(hr);

            rowcount = data.dgvData.RowCount;
            if (rowcount < max)
            {
                max = rowcount;
            }

            for (int i = min; i < max; ++i)
            {
                heartrate = Convert.ToInt32(data.dgvData.Rows[i].Cells[4].Value);

                if (heartrate >= hr6)
                {
                    z6++;
                }
                else if (heartrate >= hr5)
                {
                    z5++;
                }
                else if (heartrate >= hr4)
                {
                    z4++;
                }
                else if (heartrate >= hr3)
                {
                    z3++;
                }
                else if (heartrate >= hr2)
                {
                    z2++;
                }
                else if (heartrate >= hr1)
                {
                    z1++;
                }

            }


            CreatePie(PieChart1);
        }
        private void PieSlices()
        {
            int rowcount, heartrate = 0;
            double hr, maxh;
            data = new Form1(Fname);
            maxh = Convert.ToDouble(maxhr);


            //z1 60-65%
            hr = maxh / 100;
            hr = hr * 60;
            hr = Math.Round(hr, 0);
            hr1 = Convert.ToInt32(hr);

            //z2 65-75
            hr = maxh / 100;
            hr = hr * 65;
            hr = Math.Round(hr, 0);
            hr2 = Convert.ToInt32(hr);

            //z3 75, 82
            hr = maxh / 100;
            hr = hr * 75;
            hr = Math.Round(hr, 0);
            hr3 = Convert.ToInt32(hr);

            //z4 82, 89
            hr = maxh / 100;
            hr = hr * 82;
            hr = Math.Round(hr, 0);
            hr4 = Convert.ToInt32(hr);

            //z5 89 - 94
            hr = maxh / 100;
            hr = hr * 89;
            hr = Math.Round(hr, 0);
            hr5 = Convert.ToInt32(hr);

            //z6 94-100
            hr = maxh / 100;
            hr = hr * 94;
            hr = Math.Round(hr, 0);
            hr6 = Convert.ToInt32(hr);

            rowcount = data.dgvData.RowCount;
            if (rowcount < max)
            {
                max = rowcount;
            }

            for (int i = min; i < max; ++i)
            {
                heartrate = Convert.ToInt32(data.dgvData.Rows[i].Cells[0].Value);

                if (heartrate >= hr6)
                {
                    z6++;
                }
                else if (heartrate >= hr5)
                {
                    z5++;
                }
                else if (heartrate >= hr4)
                {
                    z4++;
                }
                else if (heartrate >= hr3)
                {
                    z3++;
                }
                else if (heartrate >= hr2)
                {
                    z2++;
                }
                else if (heartrate >= hr1)
                {
                    z1++;
                }

            }


            CreatePie(PieChart1);
        }

        public void CreatePie(ZedGraphControl zgc)
        {
            GraphPane myPane = zgc.GraphPane;

            // Set the pane title 
            if (type == "heart")
            {
                myPane.Title.Text = "Heart rate zones";
            }
            else
                myPane.Title.Text = "Power zones";

            // Fill the pane and axis background with solid color 
            myPane.Fill = new Fill(Color.White);
            myPane.Chart.Fill = new Fill(Color.White);
            myPane.Legend.Position = LegendPos.Right;

            // Create some pie slices 
            PieItem segment1 = myPane.AddPieSlice(z6, Color.Red, .1, "Zone 6");
            PieItem segment2 = myPane.AddPieSlice(z5, Color.DarkOrange, .1, "Zone 5");
            PieItem segment3 = myPane.AddPieSlice(z4, Color.Goldenrod, .1, "Zone 4");
            PieItem segment4 = myPane.AddPieSlice(z3, Color.LimeGreen, .1, "Zone 3");
            PieItem segment5 = myPane.AddPieSlice(z2, Color.Aquamarine, .1, "Zone 2");
            PieItem segment6 = myPane.AddPieSlice(z1, Color.Teal, .1, "Zone 1");
  
            segment1.LabelType = PieLabelType.Name_Percent;
            segment2.LabelType = PieLabelType.Name_Percent;
            segment3.LabelType = PieLabelType.Name_Percent;
            segment4.LabelType = PieLabelType.Name_Percent;
            segment5.LabelType = PieLabelType.Name_Percent;
            segment6.LabelType = PieLabelType.Name_Percent;
            segment1.LabelDetail.FontSpec.FontColor = Color.Red;

            // Sum up the values																					 
            CurveList curves = myPane.CurveList;
            double total = 0;
            for (int x = 0; x < curves.Count; x++)
                total += ((PieItem)curves[x]).Value;

            zgc.AxisChange(); 
        }
    }
}
