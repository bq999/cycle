﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace CycleAssignment
{
    public partial class Form1 : Form
    {
        //Variables from the header
        string Fname = @"I:\ASEb\test.txt";
        string version = "";
        string smode = "";
        string stime = "";
        string length = "";
        string interval = "";
        int count = 0;
       
       // string[] dataarray;
         string datestring = "";
        double intervald, speed = 0;
        string speedunits, distanceunits = "";
        DateTime MyTime = new DateTime();

        public static class DataContainer
        {
            public static DateTime StartTime;
        }

        public Form1(string filename)
        {
            InitializeComponent();
            Fname = filename;
            readfile(filename);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    
        public void dategrabber(string date)
        {
            int year, month, day;

            year = Convert.ToInt32(date.Substring(0, 4));
            month = Convert.ToInt32(date.Substring(4, 2));
            day = Convert.ToInt32(date.Substring(6, 2));
            DateTime datet = new DateTime(year, month, day);
            datestring = datet.ToString();

            int point = datestring.IndexOf(":");
            if (point > 3)
            {
                datestring = datestring.Substring(0, point - 3);
            }
            txtDate.Text = datestring;
        }


        
        private void btnread_Click(object sender, EventArgs e)
        {
            
            if (File.Exists(@"I:\ASEb\test.txt")){
            } try  {
                //Catch all runetime errors
                //string filecontent = System.IO.File.ReadAllText(@"I:\ASEb\txt.txt");
                //Reading text using an array
                string[] filelines = System.IO.File.ReadAllLines(@"I:\ASEb\test.txt");
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could find file");
            }

        }

        private void smodespeed(string smode)
        {
            //Substring kph 7,1
            speedunits = smode.Substring(7,1);
            if (speedunits == "0")
            {
                speedunits = " km/h";
                distanceunits = " km";
                //Change DGV column name
            }
            else
            {
                speedunits = " mp/h";
                distanceunits = " miles";


            }
            dgvcolumns();

        }

        private void dgvcolumns()
        {
            dgvData.ColumnCount = 8;
            dgvData.ColumnHeadersVisible = true;

            dgvData.Columns[0].Name = "Heart rate";
            //Apply appropiate measurements dependent on selection
            dgvData.Columns[1].Name = "Speed" + speedunits;
            dgvData.Columns[2].Name = "Cadence";
            dgvData.Columns[3].Name = "Altitude";
            dgvData.Columns[4].Name = "Power";
            dgvData.Columns[5].Name = "Power balance";
            dgvData.Columns[6].Name = "Time";
            dgvData.Columns[7].Name = "Date";

        }

        public void readfile(string filename)
        {
            string dt;
            int point;
            if (File.Exists(filename))
            {
            } try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                   
                    string line;
                    bool hrdata = false;
                    // Read and display lines from the file until the end of  
                    // the file is reached. 


                    while ((line = sr.ReadLine()) != null)
                    {
               
                        //Only adds to the listbox if it is data
                        if (hrdata == true)
                        {
                            dgvadder(line, count);
                            count = count + 1;
                        }

                        point = line.IndexOf("=");
                        //Checks for the data
                        if (line.Contains("HRData"))
                        {
                            hrdata = true;
                        } 

                        //Smode grabber
                        if (line.Contains("SMode"))
                        {
                            smode = line.Substring(point + 1);
                            txtsmode.Text = smode;
                            smodespeed(smode);
                        }

                        if (line.Contains("Monitor"))
                        {
                            
                            string monitor = line.Substring(point + 1);
                            txtmonitor.Text = monitor;
                        }

                        //Interval grabber
                        if (line.Contains("Interval"))
                        {
                            interval = line.Substring(point + 1);
                            txtinterval.Text = interval;
                            intervald = Convert.ToDouble(interval);
                        }

                        //Version grabber
                        if (line.Contains("Version"))
                        {
                            version = line.Substring(point + 1);
                            txtVersion.Text = version;

                        }

                        //Start time grabber
                        if (line.Contains("StartTime"))
                        {
                            stime = line.Substring(point + 1);
                            txtTime.Text = stime;
                            string format = @"HH\:mm\:ss\.f";
                            MyTime = DateTime.ParseExact(stime, format, null);
                            DataContainer.StartTime = MyTime;

                        }

                        //Length grabber
                        if (line.Contains("Length"))
                        {
                            length = line.Substring(point + 1);
                            txtLength.Text = length;

                        }

                        //Checks for the date
                        if (line.Contains("Date="))
                        {
                            //Grabs the integers after the =                           
                            dt = line.Substring(point + 1);
                            //Passes the date over to be processed
                           
                            dategrabber(dt);
                        }
                    }
                    //Removes unwanted text at top of listbox
                   
                    //Accounts for deleting the false value
                    count = count - 1;
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find file");
            }
        }

        private void dgvadder(string line, int count)
        {
            int datapoint = line.IndexOf("\t");
            int i = 0;
            string stringtime, stringdate, speedstring = "";
            string HRsub, speedsub, cadencesub, altsub, powersub, pbsub = "";

            //See if user activated each set of data
            //0 = off      1 = on
            HRsub = smode.Substring(6, 1);
            speedsub = smode.Substring(0, 1);
            cadencesub = smode.Substring(1, 1);
            altsub = smode.Substring(2, 1);
            powersub = smode.Substring(3, 1);
            pbsub = smode.Substring(4, 1);

           // line = dataarray[count];
            string[] dataarray;
            if (HRsub == "1")
            {
                dataarray = line.Split(new string[] { "\t" }, StringSplitOptions.None);
            }
            else
            {
                dataarray = line.Split(new string[] { "" }, StringSplitOptions.None);
            }

            string[] time;

            //Removing date from the time and to be outputted
            stringtime = MyTime.ToString();
            time = stringtime.Split(' ');
            stringtime = time[1];
            stringdate = time[0];

      
            dgvData.Columns[3].Name = "Altitude meters";

    
            //Assigns values to be inputted

            DataGridViewRow row = (DataGridViewRow)dgvData.Rows[count].Clone();
            if (HRsub == "1")
            {
                row.Cells[0].Value = dataarray[i];
                i++;
            }
            else
            {
                row.Cells[0].Value = 0;
            }

            if (speedsub == "1")
            {
                //Converting the speed 
                speed = Convert.ToDouble(dataarray[i]);
                speed = speed / 10;
                speedstring = speed.ToString();
                row.Cells[1].Value = speed;
                i++;
            }
            else
            {
                row.Cells[1].Value = 0;
            }

            if (cadencesub == "1")
            {
                row.Cells[2].Value = dataarray[i];
                i++;
            }
            else
            {
                row.Cells[2].Value = 0;
            }

            if (altsub == "1")
            {
                row.Cells[3].Value = dataarray[i];
                i++;
            }
            else
            {
                row.Cells[3].Value = 0;
            }

            if (powersub == "1")
            {
                row.Cells[4].Value = dataarray[i];
                i++;
            }
            else
            {
                row.Cells[4].Value = 0;
            }

            if (pbsub == "1")
            {
                row.Cells[5].Value = dataarray[i];
                i++;
            }
            else
            {
                row.Cells[5].Value = 0;
            }

            row.Cells[6].Value = stringtime;
            row.Cells[7].Value = stringdate;
            //Inputs values into dgv
            dgvData.Rows.Add(row);
            //Adds interval to time
            MyTime = MyTime.AddSeconds(intervald);
        }

      

        private void toolstripopen_Click(object sender, EventArgs e)
        {

        }

        public void openfile()
        {

            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Open Text File";

            theDialog.InitialDirectory = @"I:\ASEb\";
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                //Assigns the file name to a global string then reads the file
                Fname = theDialog.FileName.ToString();
                readfile(Fname);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openfile();
        }

        private void btnDate_Click(object sender, EventArgs e)
        {
         
        }

        private void unitchanger(string speedunits, bool isChecked)
        {
            dgvData.Columns[1].Name = "Speed" + speedunits;

            double multiplier = 0;

            //Setting the multiplier 
            if (isChecked == true)
            {
                multiplier = 0.621371192;
            }
            else 
            {
                multiplier = 1.609344;

            }

            double sum = 0;
            //Changes speed values
            for (int i = 0; i < dgvData.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(dgvData.Rows[i].Cells[1].Value)*multiplier;
                sum = Math.Round(sum, 1);
                dgvData.Rows[i].Cells[1].Value = sum.ToString();
                sum = 0;

            }

           
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public double totalDistance()
        {
            //Total speed / 3600
            double tdistance = 0;
            for (int i = 0; i < dgvData.Rows.Count; ++i)
            {
                tdistance += Convert.ToDouble(dgvData.Rows[i].Cells[1].Value);
            }
            tdistance = tdistance / 3600;
            tdistance = Math.Round(tdistance, 2);
            return tdistance;            
        }

        public double avgspeed()
        {
            double sum = 0;
            int j = 0;
            for (int i = 0; i < dgvData.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(dgvData.Rows[i].Cells[1].Value);
                j++;
            }
            sum = sum / j;
            sum = Math.Round(sum, 2);
            return sum;
        }

        public double avgheart()
        {
            double sum = 0;
            int j = 0;
            for (int i = 0; i < dgvData.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(dgvData.Rows[i].Cells[0].Value);
                j++;
            }
            sum = sum / j;
            sum = Math.Round(sum, 2);
            return sum;
        }

        public double avgpower()
        {
            double sum = 0;
            int j = 0;
            for (int i = 0; i < dgvData.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(dgvData.Rows[i].Cells[4].Value);
                j++;
            }
            sum = sum / j;
            sum = Math.Round(sum, 2);
            return sum;
        }

        public double Maxspeed()
        {
            int columnindex = 1;
            Double maxspeed = dgvData.Rows.Cast<DataGridViewRow>().Max(r => Convert.ToDouble(r.Cells[columnindex].Value));
            return maxspeed;
        }

        public int Maxalt()
        {
            int columnindex = 3;
            int maxalt = dgvData.Rows.Cast<DataGridViewRow>().Max(r => Convert.ToInt32(r.Cells[columnindex].Value));
            return maxalt;
        }

        public double Minheart()
        { 
               int columnindex = 0;
            Double minheart = dgvData.Rows.Cast<DataGridViewRow>().Max(r => Convert.ToDouble(r.Cells[columnindex].Value));
            //MessageBox.Show("Minimum heart rate: " + maxheart.ToString());
            string heart = "";
            foreach (DataGridViewRow Rows in dgvData.Rows)
            {
                heart = (string)Rows.Cells[0].Value;
                if (Convert.ToInt32(heart) < minheart && Convert.ToInt32(heart) != 0)
                {
                    minheart = Convert.ToInt32(heart);
                    return minheart;
                }
                return minheart;
            }
            return minheart;
        }

        public double Maxheart()
        {
            int columnindex = 0;
            Double maxheart = dgvData.Rows.Cast<DataGridViewRow>().Max(r => Convert.ToDouble(r.Cells[columnindex].Value));
            return maxheart;
        }

        public double avgAlt()
        {
            double sum = 0;
            int j = 0;
            for (int i = 0; i < dgvData.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(dgvData.Rows[i].Cells[3].Value);
                j++;
            }
            sum = sum / j;
            sum = Math.Round(sum);
            return sum;
        }

        public double Maxpower()
        {
            int columnindex = 4;
            Double maxpower = dgvData.Rows.Cast<DataGridViewRow>().Max(r => Convert.ToDouble(r.Cells[columnindex].Value));
            return maxpower;
        }

        private void totalDistanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double tdistance = totalDistance();
            MessageBox.Show("Total distance: " + tdistance.ToString() + distanceunits);
        }

        private void averageSpeedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double sum = avgspeed();
            MessageBox.Show("Average speed: " + sum.ToString() + speedunits);
        }

        private void averageHeartRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double sum = avgheart();
            MessageBox.Show("Average heart rate: " + sum.ToString());
        }

        private void averagePowerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double sum = avgpower();
            MessageBox.Show("Average power: " + sum.ToString());
        }

        private void maximumSpeedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double maxspeed = Maxspeed();
            MessageBox.Show("Maximum speed: " + maxspeed.ToString() + speedunits);
        }

        private void maximumAltitudeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int maxalt = Maxalt();
            MessageBox.Show("Maximum Altitude: " + maxalt.ToString());
        }

        private void minimumHeartRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Double minheart = Minheart();
            MessageBox.Show("Minimum heart rate: " + minheart);         

        }

        private void maximumHeartRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double maxheart = Maxheart();
            MessageBox.Show("Maximum heart rate: " + maxheart.ToString());
        }

        private void averageAltitudeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double sum = avgAlt();
            MessageBox.Show("Average altitude: " + sum.ToString());
        }

        private void maximumPowerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double maxpower = Maxpower();
            MessageBox.Show("Maximum power: " + maxpower.ToString() + " watts");
        }

        private void rbUS_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = rbUS.Checked;
            if (isChecked)
            {
                speedunits = " mp/h";
                distanceunits = " miles";
            }
            else
            {
                speedunits = " kp/h";
                distanceunits = " km";
            }
            unitchanger(speedunits, isChecked);
        }

        private void summaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Add code here
        }

        private void lbAvghr_Click(object sender, EventArgs e)
        {

        }

        private void lbMaxspeed_Click(object sender, EventArgs e)
        {

        }

        private void lbAverage_Click(object sender, EventArgs e)
        {

        }

        private void lbDistance_Click(object sender, EventArgs e)
        {

        }

        private void lbMaxhr_Click(object sender, EventArgs e)
        {

        }

        private void lbMaxalt_Click(object sender, EventArgs e)
        {

        }

        private void lbAvgalt_Click(object sender, EventArgs e)
        {

        }

        private void lbMaxpwr_Click(object sender, EventArgs e)
        {

        }

        private void lbAvgpwr_Click(object sender, EventArgs e)
        {

        }

        private void lbMinhr_Click(object sender, EventArgs e)
        {

        }

    }

    public class DateMaker
    {
        public string Dateoutput(string datestring)
        {
            int year, month, day;
            if (datestring.Length != 8)
            {
                datestring = "Invalid Length";
                return datestring;
            }
            else
            {
                year = Convert.ToInt32(datestring.Substring(0, 4));
                month = Convert.ToInt32(datestring.Substring(4, 2));
                day = Convert.ToInt32(datestring.Substring(6, 2));
                DateTime datet = new DateTime(year, month, day);
                datestring = datet.ToString();
                //MessageBox.Show("Date: " + datet + smode + version);

                int point = datestring.IndexOf(":");
                if (point > 3)
                {
                    datestring = datestring.Substring(0, point - 3);
                }
                return datestring;
            }
        }
    }

    public class smodetester
    {
        public string smodetest(string smode)
        {
            string distanceunits, speedunits = "";
            //Substring kph 7,1
            speedunits = smode.Substring(7, 1);
            if (speedunits == "0")
            {
                speedunits = " km/h";
                distanceunits = " km";
                //Change DGV column name
            }
            else
            {
                speedunits = " mp/h";
                distanceunits = " miles";
            }

            return distanceunits;
        }
    }

    public class units
    {
        public double unitsetter(bool isChecked)
        {

            double multiplier = 0;
            //Setting the multiplier 
            if (isChecked == true)
            {
                multiplier = 0.621371192;
            }
            else
            {
                multiplier = 1.609344;

            }

            return multiplier;
        }
    }
}

