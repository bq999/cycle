﻿namespace CycleAssignment
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calendarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allCalculationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.powerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altitudeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtMaxspeed = new System.Windows.Forms.TextBox();
            this.txtAvgHR = new System.Windows.Forms.TextBox();
            this.txtAvgspeed = new System.Windows.Forms.TextBox();
            this.txtDistance = new System.Windows.Forms.TextBox();
            this.txtMaxAlt = new System.Windows.Forms.TextBox();
            this.txtAvgalt = new System.Windows.Forms.TextBox();
            this.txtMin = new System.Windows.Forms.TextBox();
            this.txtMax = new System.Windows.Forms.TextBox();
            this.txtAvgpwr = new System.Windows.Forms.TextBox();
            this.txtMaxpwr = new System.Windows.Forms.TextBox();
            this.lblMinX = new System.Windows.Forms.Label();
            this.lblMaxX = new System.Windows.Forms.Label();
            this.lblDistance = new System.Windows.Forms.Label();
            this.lblMaxalt = new System.Windows.Forms.Label();
            this.lblMaxspeed = new System.Windows.Forms.Label();
            this.lblAvgHr = new System.Windows.Forms.Label();
            this.lblavgspeed = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAveragepwr = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMinHr = new System.Windows.Forms.TextBox();
            this.txtMaxHr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.datePicker = new System.Windows.Forms.MonthCalendar();
            this.cbHR = new System.Windows.Forms.CheckBox();
            this.cbspeed = new System.Windows.Forms.CheckBox();
            this.cbcadence = new System.Windows.Forms.CheckBox();
            this.cbpower = new System.Windows.Forms.CheckBox();
            this.cbaltitude = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblDatepicker = new System.Windows.Forms.Label();
            this.panelmetrics = new System.Windows.Forms.Panel();
            this.btnRecalc = new System.Windows.Forms.Button();
            this.txtIF = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTSS = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cBNP = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblinterval = new System.Windows.Forms.Label();
            this.txtIntervals = new System.Windows.Forms.TextBox();
            this.btnfwd = new System.Windows.Forms.Button();
            this.btnbkwd = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.txtfname = new System.Windows.Forms.TextBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.lblsummary = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.calculateTSS = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.ftpEnter = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.yourFTP = new System.Windows.Forms.Label();
            this.yourNP = new System.Windows.Forms.Label();
            this.yourIF = new System.Windows.Forms.Label();
            this.yourTSS = new System.Windows.Forms.Label();
            this.btnpowerzone = new System.Windows.Forms.Button();
            this.txtpwrzone = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnheartzone = new System.Windows.Forms.Button();
            this.txtHrzone = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelmetrics.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(12, 27);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(1431, 487);
            this.zedGraphControl1.TabIndex = 0;
            this.zedGraphControl1.Resize += new System.EventHandler(this.zedGraphControl1_Resize);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dataToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1455, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewDataToolStripMenuItem,
            this.openToolStripMenuItem,
            this.calendarToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // viewDataToolStripMenuItem
            // 
            this.viewDataToolStripMenuItem.Name = "viewDataToolStripMenuItem";
            this.viewDataToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.viewDataToolStripMenuItem.Text = "View data";
            this.viewDataToolStripMenuItem.Click += new System.EventHandler(this.viewDataToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // calendarToolStripMenuItem
            // 
            this.calendarToolStripMenuItem.Name = "calendarToolStripMenuItem";
            this.calendarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.calendarToolStripMenuItem.Text = "Calendar";
            this.calendarToolStripMenuItem.Click += new System.EventHandler(this.viewCalendarToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allCalculationsToolStripMenuItem,
            this.speedToolStripMenuItem,
            this.cadenceToolStripMenuItem,
            this.powerToolStripMenuItem,
            this.altitudeToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // allCalculationsToolStripMenuItem
            // 
            this.allCalculationsToolStripMenuItem.Name = "allCalculationsToolStripMenuItem";
            this.allCalculationsToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.allCalculationsToolStripMenuItem.Text = "Heart rate";
            this.allCalculationsToolStripMenuItem.Click += new System.EventHandler(this.allCalculationsToolStripMenuItem_Click);
            // 
            // speedToolStripMenuItem
            // 
            this.speedToolStripMenuItem.Name = "speedToolStripMenuItem";
            this.speedToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.speedToolStripMenuItem.Text = "Speed";
            this.speedToolStripMenuItem.Click += new System.EventHandler(this.speedToolStripMenuItem_Click);
            // 
            // cadenceToolStripMenuItem
            // 
            this.cadenceToolStripMenuItem.Name = "cadenceToolStripMenuItem";
            this.cadenceToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.cadenceToolStripMenuItem.Text = "Cadence";
            this.cadenceToolStripMenuItem.Click += new System.EventHandler(this.cadenceToolStripMenuItem_Click);
            // 
            // powerToolStripMenuItem
            // 
            this.powerToolStripMenuItem.Name = "powerToolStripMenuItem";
            this.powerToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.powerToolStripMenuItem.Text = "Power";
            this.powerToolStripMenuItem.Click += new System.EventHandler(this.powerToolStripMenuItem_Click);
            // 
            // altitudeToolStripMenuItem
            // 
            this.altitudeToolStripMenuItem.Name = "altitudeToolStripMenuItem";
            this.altitudeToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.altitudeToolStripMenuItem.Text = "Altitude";
            this.altitudeToolStripMenuItem.Click += new System.EventHandler(this.altitudeToolStripMenuItem_Click);
            // 
            // txtMaxspeed
            // 
            this.txtMaxspeed.Location = new System.Drawing.Point(15, 163);
            this.txtMaxspeed.Name = "txtMaxspeed";
            this.txtMaxspeed.Size = new System.Drawing.Size(100, 20);
            this.txtMaxspeed.TabIndex = 2;
            // 
            // txtAvgHR
            // 
            this.txtAvgHR.Location = new System.Drawing.Point(227, 71);
            this.txtAvgHR.Name = "txtAvgHR";
            this.txtAvgHR.Size = new System.Drawing.Size(100, 20);
            this.txtAvgHR.TabIndex = 2;
            // 
            // txtAvgspeed
            // 
            this.txtAvgspeed.Location = new System.Drawing.Point(15, 116);
            this.txtAvgspeed.Name = "txtAvgspeed";
            this.txtAvgspeed.Size = new System.Drawing.Size(100, 20);
            this.txtAvgspeed.TabIndex = 2;
            // 
            // txtDistance
            // 
            this.txtDistance.Location = new System.Drawing.Point(227, 24);
            this.txtDistance.Name = "txtDistance";
            this.txtDistance.Size = new System.Drawing.Size(100, 20);
            this.txtDistance.TabIndex = 2;
            // 
            // txtMaxAlt
            // 
            this.txtMaxAlt.Location = new System.Drawing.Point(227, 163);
            this.txtMaxAlt.Name = "txtMaxAlt";
            this.txtMaxAlt.Size = new System.Drawing.Size(100, 20);
            this.txtMaxAlt.TabIndex = 2;
            // 
            // txtAvgalt
            // 
            this.txtAvgalt.Location = new System.Drawing.Point(227, 116);
            this.txtAvgalt.Name = "txtAvgalt";
            this.txtAvgalt.Size = new System.Drawing.Size(100, 20);
            this.txtAvgalt.TabIndex = 2;
            // 
            // txtMin
            // 
            this.txtMin.Location = new System.Drawing.Point(15, 24);
            this.txtMin.Name = "txtMin";
            this.txtMin.Size = new System.Drawing.Size(100, 20);
            this.txtMin.TabIndex = 2;
            this.txtMin.TextChanged += new System.EventHandler(this.txtMin_TextChanged);
            // 
            // txtMax
            // 
            this.txtMax.Location = new System.Drawing.Point(15, 71);
            this.txtMax.Name = "txtMax";
            this.txtMax.Size = new System.Drawing.Size(100, 20);
            this.txtMax.TabIndex = 2;
            // 
            // txtAvgpwr
            // 
            this.txtAvgpwr.Location = new System.Drawing.Point(121, 116);
            this.txtAvgpwr.Name = "txtAvgpwr";
            this.txtAvgpwr.Size = new System.Drawing.Size(100, 20);
            this.txtAvgpwr.TabIndex = 2;
            // 
            // txtMaxpwr
            // 
            this.txtMaxpwr.Location = new System.Drawing.Point(121, 163);
            this.txtMaxpwr.Name = "txtMaxpwr";
            this.txtMaxpwr.Size = new System.Drawing.Size(100, 20);
            this.txtMaxpwr.TabIndex = 2;
            // 
            // lblMinX
            // 
            this.lblMinX.AutoSize = true;
            this.lblMinX.Location = new System.Drawing.Point(15, 8);
            this.lblMinX.Name = "lblMinX";
            this.lblMinX.Size = new System.Drawing.Size(34, 13);
            this.lblMinX.TabIndex = 3;
            this.lblMinX.Text = "Min X";
            // 
            // lblMaxX
            // 
            this.lblMaxX.AutoSize = true;
            this.lblMaxX.Location = new System.Drawing.Point(15, 55);
            this.lblMaxX.Name = "lblMaxX";
            this.lblMaxX.Size = new System.Drawing.Size(37, 13);
            this.lblMaxX.TabIndex = 3;
            this.lblMaxX.Text = "Max X";
            // 
            // lblDistance
            // 
            this.lblDistance.AutoSize = true;
            this.lblDistance.Location = new System.Drawing.Point(224, 8);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(49, 13);
            this.lblDistance.TabIndex = 3;
            this.lblDistance.Text = "Distance";
            // 
            // lblMaxalt
            // 
            this.lblMaxalt.AutoSize = true;
            this.lblMaxalt.Location = new System.Drawing.Point(224, 147);
            this.lblMaxalt.Name = "lblMaxalt";
            this.lblMaxalt.Size = new System.Drawing.Size(64, 13);
            this.lblMaxalt.TabIndex = 3;
            this.lblMaxalt.Text = "Max altitude";
            // 
            // lblMaxspeed
            // 
            this.lblMaxspeed.AutoSize = true;
            this.lblMaxspeed.Location = new System.Drawing.Point(15, 147);
            this.lblMaxspeed.Name = "lblMaxspeed";
            this.lblMaxspeed.Size = new System.Drawing.Size(59, 13);
            this.lblMaxspeed.TabIndex = 3;
            this.lblMaxspeed.Text = "Max speed";
            // 
            // lblAvgHr
            // 
            this.lblAvgHr.AutoSize = true;
            this.lblAvgHr.Location = new System.Drawing.Point(227, 55);
            this.lblAvgHr.Name = "lblAvgHr";
            this.lblAvgHr.Size = new System.Drawing.Size(95, 13);
            this.lblAvgHr.TabIndex = 3;
            this.lblAvgHr.Text = "Average heart rate";
            // 
            // lblavgspeed
            // 
            this.lblavgspeed.AutoSize = true;
            this.lblavgspeed.Location = new System.Drawing.Point(12, 100);
            this.lblavgspeed.Name = "lblavgspeed";
            this.lblavgspeed.Size = new System.Drawing.Size(79, 13);
            this.lblavgspeed.TabIndex = 3;
            this.lblavgspeed.Text = "Average speed";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(224, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Average altitude";
            // 
            // lblAveragepwr
            // 
            this.lblAveragepwr.AutoSize = true;
            this.lblAveragepwr.Location = new System.Drawing.Point(118, 100);
            this.lblAveragepwr.Name = "lblAveragepwr";
            this.lblAveragepwr.Size = new System.Drawing.Size(79, 13);
            this.lblAveragepwr.TabIndex = 3;
            this.lblAveragepwr.Text = "Average power";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(118, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Max Power";
            // 
            // txtMinHr
            // 
            this.txtMinHr.Location = new System.Drawing.Point(121, 24);
            this.txtMinHr.Name = "txtMinHr";
            this.txtMinHr.Size = new System.Drawing.Size(100, 20);
            this.txtMinHr.TabIndex = 2;
            // 
            // txtMaxHr
            // 
            this.txtMaxHr.Location = new System.Drawing.Point(121, 71);
            this.txtMaxHr.Name = "txtMaxHr";
            this.txtMaxHr.Size = new System.Drawing.Size(100, 20);
            this.txtMaxHr.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(121, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Min heart rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(121, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Max heart rate";
            // 
            // datePicker
            // 
            this.datePicker.Location = new System.Drawing.Point(20, 26);
            this.datePicker.Name = "datePicker";
            this.datePicker.TabIndex = 4;
            this.datePicker.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.datePicker_DateChanged);
            this.datePicker.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.datePicker_DateSelected);
            // 
            // cbHR
            // 
            this.cbHR.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbHR.Checked = true;
            this.cbHR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHR.Location = new System.Drawing.Point(12, 526);
            this.cbHR.Name = "cbHR";
            this.cbHR.Size = new System.Drawing.Size(95, 95);
            this.cbHR.TabIndex = 5;
            this.cbHR.Text = "Heart rate";
            this.cbHR.UseVisualStyleBackColor = true;
            this.cbHR.CheckedChanged += new System.EventHandler(this.cbHR_CheckedChanged);
            // 
            // cbspeed
            // 
            this.cbspeed.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbspeed.Checked = true;
            this.cbspeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbspeed.Location = new System.Drawing.Point(12, 628);
            this.cbspeed.Name = "cbspeed";
            this.cbspeed.Size = new System.Drawing.Size(95, 95);
            this.cbspeed.TabIndex = 5;
            this.cbspeed.Text = "Speed";
            this.cbspeed.UseVisualStyleBackColor = true;
            this.cbspeed.CheckedChanged += new System.EventHandler(this.cbspeed_CheckedChanged);
            // 
            // cbcadence
            // 
            this.cbcadence.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbcadence.Checked = true;
            this.cbcadence.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbcadence.Location = new System.Drawing.Point(113, 628);
            this.cbcadence.Name = "cbcadence";
            this.cbcadence.Size = new System.Drawing.Size(95, 95);
            this.cbcadence.TabIndex = 5;
            this.cbcadence.Text = "Cadence";
            this.cbcadence.UseVisualStyleBackColor = true;
            this.cbcadence.CheckedChanged += new System.EventHandler(this.cbcadence_CheckedChanged);
            // 
            // cbpower
            // 
            this.cbpower.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbpower.Checked = true;
            this.cbpower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbpower.Location = new System.Drawing.Point(113, 526);
            this.cbpower.Name = "cbpower";
            this.cbpower.Size = new System.Drawing.Size(95, 95);
            this.cbpower.TabIndex = 5;
            this.cbpower.Text = "Power";
            this.cbpower.UseVisualStyleBackColor = true;
            this.cbpower.CheckedChanged += new System.EventHandler(this.cbpower_CheckedChanged);
            // 
            // cbaltitude
            // 
            this.cbaltitude.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbaltitude.Checked = true;
            this.cbaltitude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbaltitude.Location = new System.Drawing.Point(214, 526);
            this.cbaltitude.Name = "cbaltitude";
            this.cbaltitude.Size = new System.Drawing.Size(95, 95);
            this.cbaltitude.TabIndex = 5;
            this.cbaltitude.Text = "Altitude";
            this.cbaltitude.UseVisualStyleBackColor = true;
            this.cbaltitude.CheckedChanged += new System.EventHandler(this.cbaltitude_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.txtMaxAlt);
            this.panel1.Controls.Add(this.txtMaxspeed);
            this.panel1.Controls.Add(this.txtDistance);
            this.panel1.Controls.Add(this.txtMinHr);
            this.panel1.Controls.Add(this.txtMin);
            this.panel1.Controls.Add(this.txtAvgHR);
            this.panel1.Controls.Add(this.txtMaxHr);
            this.panel1.Controls.Add(this.txtMax);
            this.panel1.Controls.Add(this.txtAvgspeed);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtAvgalt);
            this.panel1.Controls.Add(this.lblAveragepwr);
            this.panel1.Controls.Add(this.txtMaxpwr);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtAvgpwr);
            this.panel1.Controls.Add(this.lblavgspeed);
            this.panel1.Controls.Add(this.lblMinX);
            this.panel1.Controls.Add(this.lblAvgHr);
            this.panel1.Controls.Add(this.lblMaxX);
            this.panel1.Controls.Add(this.lblMaxspeed);
            this.panel1.Controls.Add(this.lblDistance);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblMaxalt);
            this.panel1.Location = new System.Drawing.Point(1101, 526);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(342, 197);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel2.Controls.Add(this.lblDatepicker);
            this.panel2.Controls.Add(this.datePicker);
            this.panel2.Location = new System.Drawing.Point(481, 527);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(265, 197);
            this.panel2.TabIndex = 8;
            // 
            // lblDatepicker
            // 
            this.lblDatepicker.AutoSize = true;
            this.lblDatepicker.Location = new System.Drawing.Point(20, 8);
            this.lblDatepicker.Name = "lblDatepicker";
            this.lblDatepicker.Size = new System.Drawing.Size(70, 13);
            this.lblDatepicker.TabIndex = 5;
            this.lblDatepicker.Text = "Select a date";
            // 
            // panelmetrics
            // 
            this.panelmetrics.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelmetrics.Controls.Add(this.btnRecalc);
            this.panelmetrics.Controls.Add(this.txtIF);
            this.panelmetrics.Controls.Add(this.label5);
            this.panelmetrics.Controls.Add(this.txtNP);
            this.panelmetrics.Controls.Add(this.label4);
            this.panelmetrics.Controls.Add(this.txtTSS);
            this.panelmetrics.Controls.Add(this.label3);
            this.panelmetrics.Location = new System.Drawing.Point(958, 526);
            this.panelmetrics.Name = "panelmetrics";
            this.panelmetrics.Size = new System.Drawing.Size(137, 198);
            this.panelmetrics.TabIndex = 11;
            // 
            // btnRecalc
            // 
            this.btnRecalc.Location = new System.Drawing.Point(15, 172);
            this.btnRecalc.Name = "btnRecalc";
            this.btnRecalc.Size = new System.Drawing.Size(105, 23);
            this.btnRecalc.TabIndex = 2;
            this.btnRecalc.Text = "Reload NP IF TSS";
            this.btnRecalc.UseVisualStyleBackColor = true;
            this.btnRecalc.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtIF
            // 
            this.txtIF.Location = new System.Drawing.Point(15, 146);
            this.txtIF.Name = "txtIF";
            this.txtIF.Size = new System.Drawing.Size(105, 20);
            this.txtIF.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Intensity Factor";
            // 
            // txtNP
            // 
            this.txtNP.Location = new System.Drawing.Point(15, 95);
            this.txtNP.Name = "txtNP";
            this.txtNP.Size = new System.Drawing.Size(105, 20);
            this.txtNP.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Normalized Power";
            // 
            // txtTSS
            // 
            this.txtTSS.Location = new System.Drawing.Point(15, 40);
            this.txtTSS.Name = "txtTSS";
            this.txtTSS.Size = new System.Drawing.Size(105, 20);
            this.txtTSS.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Training Stress Score";
            // 
            // cBNP
            // 
            this.cBNP.Appearance = System.Windows.Forms.Appearance.Button;
            this.cBNP.Checked = true;
            this.cBNP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBNP.Location = new System.Drawing.Point(213, 628);
            this.cBNP.Name = "cBNP";
            this.cBNP.Size = new System.Drawing.Size(96, 96);
            this.cBNP.TabIndex = 12;
            this.cBNP.Text = "Normalised Power";
            this.cBNP.UseVisualStyleBackColor = true;
            this.cBNP.CheckedChanged += new System.EventHandler(this.cBNP_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel5.Controls.Add(this.lblinterval);
            this.panel5.Controls.Add(this.txtIntervals);
            this.panel5.Controls.Add(this.btnfwd);
            this.panel5.Controls.Add(this.btnbkwd);
            this.panel5.Location = new System.Drawing.Point(315, 527);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(160, 67);
            this.panel5.TabIndex = 13;
            // 
            // lblinterval
            // 
            this.lblinterval.AutoSize = true;
            this.lblinterval.Location = new System.Drawing.Point(37, 5);
            this.lblinterval.Name = "lblinterval";
            this.lblinterval.Size = new System.Drawing.Size(74, 13);
            this.lblinterval.TabIndex = 2;
            this.lblinterval.Text = "Interval picker";
            // 
            // txtIntervals
            // 
            this.txtIntervals.Location = new System.Drawing.Point(19, 42);
            this.txtIntervals.Name = "txtIntervals";
            this.txtIntervals.Size = new System.Drawing.Size(126, 20);
            this.txtIntervals.TabIndex = 1;
            // 
            // btnfwd
            // 
            this.btnfwd.Location = new System.Drawing.Point(85, 19);
            this.btnfwd.Name = "btnfwd";
            this.btnfwd.Size = new System.Drawing.Size(60, 20);
            this.btnfwd.TabIndex = 0;
            this.btnfwd.Text = ">>";
            this.btnfwd.UseVisualStyleBackColor = true;
            this.btnfwd.Click += new System.EventHandler(this.btnfwd_Click);
            // 
            // btnbkwd
            // 
            this.btnbkwd.Location = new System.Drawing.Point(19, 18);
            this.btnbkwd.Name = "btnbkwd";
            this.btnbkwd.Size = new System.Drawing.Size(60, 20);
            this.btnbkwd.TabIndex = 0;
            this.btnbkwd.Text = "<<";
            this.btnbkwd.UseVisualStyleBackColor = true;
            this.btnbkwd.Click += new System.EventHandler(this.btnbkwd_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel6.Controls.Add(this.btnLoad);
            this.panel6.Controls.Add(this.btnNext);
            this.panel6.Controls.Add(this.btnBack);
            this.panel6.Controls.Add(this.txtfname);
            this.panel6.Controls.Add(this.lblTime);
            this.panel6.Controls.Add(this.txtDate);
            this.panel6.Controls.Add(this.lblsummary);
            this.panel6.Controls.Add(this.txtTime);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Location = new System.Drawing.Point(315, 600);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(160, 124);
            this.panel6.TabIndex = 14;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(44, 92);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 13;
            this.btnLoad.Text = "Load file";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(124, 92);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(30, 23);
            this.btnNext.TabIndex = 12;
            this.btnNext.Text = ">>";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(8, 92);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(30, 23);
            this.btnBack.TabIndex = 12;
            this.btnBack.Text = "<<";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // txtfname
            // 
            this.txtfname.Location = new System.Drawing.Point(8, 23);
            this.txtfname.Name = "txtfname";
            this.txtfname.Size = new System.Drawing.Size(146, 20);
            this.txtfname.TabIndex = 11;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(86, 50);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(30, 13);
            this.lblTime.TabIndex = 10;
            this.lblTime.Text = "Time";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(8, 66);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(72, 20);
            this.txtDate.TabIndex = 9;
            // 
            // lblsummary
            // 
            this.lblsummary.AutoSize = true;
            this.lblsummary.Location = new System.Drawing.Point(8, 50);
            this.lblsummary.Name = "lblsummary";
            this.lblsummary.Size = new System.Drawing.Size(30, 13);
            this.lblsummary.TabIndex = 7;
            this.lblsummary.Text = "Date";
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(86, 66);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(68, 20);
            this.txtTime.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(52, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "File details";
            // 
            // calculateTSS
            // 
            this.calculateTSS.Location = new System.Drawing.Point(862, 756);
            this.calculateTSS.Name = "calculateTSS";
            this.calculateTSS.Size = new System.Drawing.Size(97, 23);
            this.calculateTSS.TabIndex = 80;
            this.calculateTSS.Text = "Calculate TSS";
            this.calculateTSS.UseVisualStyleBackColor = true;
            this.calculateTSS.Click += new System.EventHandler(this.calculateTSS_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.SystemColors.Menu;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(1084, 788);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(145, 20);
            this.label34.TabIndex = 79;
            this.label34.Text = "Intensity Factor: ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.SystemColors.Menu;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(288, 788);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(94, 20);
            this.label33.TabIndex = 78;
            this.label33.Text = "Your FTP: ";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.SystemColors.Menu;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(798, 788);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(162, 20);
            this.label32.TabIndex = 77;
            this.label32.Text = "Normalized Power: ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.SystemColors.Menu;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(490, 788);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(192, 20);
            this.label31.TabIndex = 76;
            this.label31.Text = "Training Stress Score: ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.SystemColors.Menu;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(587, 758);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(143, 20);
            this.label30.TabIndex = 75;
            this.label30.Text = "Enter Your FTP: ";
            // 
            // ftpEnter
            // 
            this.ftpEnter.AcceptsReturn = true;
            this.ftpEnter.Location = new System.Drawing.Point(729, 758);
            this.ftpEnter.Name = "ftpEnter";
            this.ftpEnter.Size = new System.Drawing.Size(126, 20);
            this.ftpEnter.TabIndex = 74;
            this.ftpEnter.TextChanged += new System.EventHandler(this.ftpEnter_TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.SystemColors.Info;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(129, 728);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(114, 20);
            this.label29.TabIndex = 72;
            this.label29.Text = "Extra Metrics";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBox2.Location = new System.Drawing.Point(125, 748);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(1264, 95);
            this.richTextBox2.TabIndex = 73;
            this.richTextBox2.Text = "";
            // 
            // yourFTP
            // 
            this.yourFTP.AutoSize = true;
            this.yourFTP.Location = new System.Drawing.Point(388, 793);
            this.yourFTP.Name = "yourFTP";
            this.yourFTP.Size = new System.Drawing.Size(0, 13);
            this.yourFTP.TabIndex = 81;
            // 
            // yourNP
            // 
            this.yourNP.AutoSize = true;
            this.yourNP.Location = new System.Drawing.Point(973, 792);
            this.yourNP.Name = "yourNP";
            this.yourNP.Size = new System.Drawing.Size(0, 13);
            this.yourNP.TabIndex = 82;
            // 
            // yourIF
            // 
            this.yourIF.AutoSize = true;
            this.yourIF.Location = new System.Drawing.Point(1245, 792);
            this.yourIF.Name = "yourIF";
            this.yourIF.Size = new System.Drawing.Size(0, 13);
            this.yourIF.TabIndex = 83;
            // 
            // yourTSS
            // 
            this.yourTSS.AutoSize = true;
            this.yourTSS.Location = new System.Drawing.Point(688, 793);
            this.yourTSS.Name = "yourTSS";
            this.yourTSS.Size = new System.Drawing.Size(0, 13);
            this.yourTSS.TabIndex = 84;
            // 
            // btnpowerzone
            // 
            this.btnpowerzone.Location = new System.Drawing.Point(18, 46);
            this.btnpowerzone.Name = "btnpowerzone";
            this.btnpowerzone.Size = new System.Drawing.Size(165, 33);
            this.btnpowerzone.TabIndex = 6;
            this.btnpowerzone.Text = "Power zone";
            this.btnpowerzone.UseVisualStyleBackColor = true;
            this.btnpowerzone.Click += new System.EventHandler(this.btnpowerzone_Click);
            // 
            // txtpwrzone
            // 
            this.txtpwrzone.Location = new System.Drawing.Point(18, 16);
            this.txtpwrzone.Name = "txtpwrzone";
            this.txtpwrzone.Size = new System.Drawing.Size(165, 20);
            this.txtpwrzone.TabIndex = 7;
            this.txtpwrzone.Text = "Functional Threshold Power";
            this.txtpwrzone.Click += new System.EventHandler(this.txtpwrzone_Click);
            this.txtpwrzone.TextChanged += new System.EventHandler(this.txtpwrzone_TextChanged);
            this.txtpwrzone.Leave += new System.EventHandler(this.txtpwrzone_Leave);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel4.Controls.Add(this.txtpwrzone);
            this.panel4.Controls.Add(this.btnpowerzone);
            this.panel4.Location = new System.Drawing.Point(752, 627);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 98);
            this.panel4.TabIndex = 10;
            // 
            // btnheartzone
            // 
            this.btnheartzone.Location = new System.Drawing.Point(18, 45);
            this.btnheartzone.Name = "btnheartzone";
            this.btnheartzone.Size = new System.Drawing.Size(165, 33);
            this.btnheartzone.TabIndex = 6;
            this.btnheartzone.Text = "Heart rate zone";
            this.btnheartzone.UseVisualStyleBackColor = true;
            this.btnheartzone.Click += new System.EventHandler(this.btnheartzone_Click);
            // 
            // txtHrzone
            // 
            this.txtHrzone.Location = new System.Drawing.Point(18, 17);
            this.txtHrzone.Name = "txtHrzone";
            this.txtHrzone.Size = new System.Drawing.Size(165, 20);
            this.txtHrzone.TabIndex = 7;
            this.txtHrzone.Text = "Max heart rate";
            this.txtHrzone.Click += new System.EventHandler(this.txtHrzone_Click);
            this.txtHrzone.Leave += new System.EventHandler(this.txtHrzone_Leave);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel3.Controls.Add(this.txtHrzone);
            this.panel3.Controls.Add(this.btnheartzone);
            this.panel3.Location = new System.Drawing.Point(752, 527);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 95);
            this.panel3.TabIndex = 9;
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1455, 993);
            this.Controls.Add(this.yourTSS);
            this.Controls.Add(this.yourIF);
            this.Controls.Add(this.yourNP);
            this.Controls.Add(this.yourFTP);
            this.Controls.Add(this.calculateTSS);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.ftpEnter);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.cBNP);
            this.Controls.Add(this.panelmetrics);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cbaltitude);
            this.Controls.Add(this.cbpower);
            this.Controls.Add(this.cbcadence);
            this.Controls.Add(this.cbspeed);
            this.Controls.Add(this.cbHR);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GraphForm";
            this.Text = "GraphForm";
            this.Load += new System.EventHandler(this.GraphForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelmetrics.ResumeLayout(false);
            this.panelmetrics.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.TextBox txtMaxspeed;
        private System.Windows.Forms.TextBox txtAvgHR;
        private System.Windows.Forms.TextBox txtAvgspeed;
        private System.Windows.Forms.TextBox txtDistance;
        private System.Windows.Forms.TextBox txtMaxAlt;
        private System.Windows.Forms.TextBox txtAvgalt;
        private System.Windows.Forms.TextBox txtMin;
        private System.Windows.Forms.TextBox txtMax;
        private System.Windows.Forms.TextBox txtAvgpwr;
        private System.Windows.Forms.TextBox txtMaxpwr;
        private System.Windows.Forms.Label lblMinX;
        private System.Windows.Forms.Label lblMaxX;
        private System.Windows.Forms.Label lblDistance;
        private System.Windows.Forms.Label lblMaxalt;
        private System.Windows.Forms.Label lblMaxspeed;
        private System.Windows.Forms.Label lblAvgHr;
        private System.Windows.Forms.Label lblavgspeed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblAveragepwr;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMinHr;
        private System.Windows.Forms.TextBox txtMaxHr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MonthCalendar datePicker;
        private System.Windows.Forms.CheckBox cbHR;
        private System.Windows.Forms.CheckBox cbspeed;
        private System.Windows.Forms.CheckBox cbcadence;
        private System.Windows.Forms.CheckBox cbpower;
        private System.Windows.Forms.CheckBox cbaltitude;
        private System.Windows.Forms.ToolStripMenuItem allCalculationsToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem speedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altitudeToolStripMenuItem;
        private System.Windows.Forms.Panel panelmetrics;
        private System.Windows.Forms.TextBox txtIF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTSS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDatepicker;
        private System.Windows.Forms.CheckBox cBNP;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblinterval;
        private System.Windows.Forms.TextBox txtIntervals;
        private System.Windows.Forms.Button btnfwd;
        private System.Windows.Forms.Button btnbkwd;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtfname;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label lblsummary;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnRecalc;
        private System.Windows.Forms.ToolStripMenuItem calendarToolStripMenuItem;
        private System.Windows.Forms.Button calculateTSS;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox ftpEnter;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label yourFTP;
        private System.Windows.Forms.Label yourNP;
        private System.Windows.Forms.Label yourIF;
        private System.Windows.Forms.Label yourTSS;
        private System.Windows.Forms.Button btnpowerzone;
        private System.Windows.Forms.TextBox txtpwrzone;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnheartzone;
        private System.Windows.Forms.TextBox txtHrzone;
        private System.Windows.Forms.Panel panel3;
    }
}